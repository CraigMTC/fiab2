﻿using System.Collections;
using System.Collections.Generic;
using HoloToolkit.Unity.InputModule;
using HoloToolkit.Unity.SpatialMapping;
using UnityEngine.XR.WSA;
using UnityEngine;
using UnityEngine.UI;

public class MoveByGaze : MonoBehaviour, IInputClickHandler
{
    [SerializeField]
    TMPro.TextMeshProUGUI AnchorInfo = null;
    [SerializeField]
    MtcAdv.MtcAdvObjectManipulator MTCManipulator = null;
    [SerializeField]
    GameObject ManipulationCanvas = null;
    //[SerializeField]
    
    public TestScript tester = null;

    Camera cam;

    bool Placed { get; set; }
    bool _isAnchorSet;

    const int ANCHOR_LAYER = 9;
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        if (tester != null)
        {
            if (!tester.AppSetsAnchor)
            {
                this.gameObject.SetActive(false);
                this.enabled = false;
            }
            else this.transform.GetChild(0).GetChild(1).gameObject.SetActive(false);
        }
        ManipulationCanvas.SetActive(false);
        //AnchorInfo.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (!Placed)
        {
            if (Physics.Raycast(cam.transform.position, cam.transform.forward, out RaycastHit hitInfo, Mathf.Infinity, SpatialMappingManager.Instance.LayerMask))
            {
                this.transform.position = hitInfo.point;
            }
            else this.transform.position = cam.transform.position + (cam.transform.forward * 5.0f);
            //AnchorInfo.text = "position: " + this.transform.position.ToString()
            //    + "\nScale :" + this.transform.lossyScale.ToString();
            //AnchorInfo.text = "";
        }
    }

    public void OnInputClicked(InputClickedEventData eventData)
    {
        if (!this._isAnchorSet)
        {
            Debug.Log("Clicked");
            if (!this.Placed)
            {
                Debug.Log("CAD placed");
                this.Placed = true;
                //this.gameObject.AddComponent<WorldAnchor>();
                //this.MTCManipulator.gameObject.SetActive(true);
                //this.MTCManipulator.SelectObject(this.gameObject);
                //tester.OnCreateAnchorAsync(this.gameObject);
                this.ManipulationCanvas.SetActive(true);
                Vector3 tempPos = this.transform.position;
                tempPos.y += 1.3f;
                this.ManipulationCanvas.transform.position = tempPos;
            }
            else
            {
                Debug.Log("CAD lifted");
                this.Placed = false;
                this.ManipulationCanvas.SetActive(false);
                //WorldAnchor anchor = this.GetComponent<WorldAnchor>();
                //if (anchor != null)
                //{
                //    GameObject.DestroyImmediate(anchor);
                //}
            }
        }
    }

    public void SetAnchor()
    {
        this.gameObject.AddComponent<WorldAnchor>();
        this.ManipulationCanvas.SetActive(false);
        this._isAnchorSet = true;
        tester.OnCreateAnchorAsync(this.gameObject);
    }

    public void ChangeScale(Slider multiplier)
    {
        this.transform.localScale = Vector3.one * multiplier.value;
    }

    public void ChangeRotationX(Slider axisSlider)
    {
        this.transform.rotation = Quaternion.Euler(axisSlider.value, this.transform.localEulerAngles.y, this.transform.localEulerAngles.z);
    }

    public void ChangeRotationY(Slider axisSlider)
    {
        this.transform.rotation = Quaternion.Euler(this.transform.localEulerAngles.x, axisSlider.value, this.transform.localEulerAngles.z);
    }

    public void ChangeRotationZ(Slider axisSlider)
    {
        this.transform.rotation = Quaternion.Euler(this.transform.localEulerAngles.x, this.transform.localEulerAngles.y, axisSlider.value);
    }
}
