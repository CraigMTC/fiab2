﻿using Microsoft.Azure.SpatialAnchors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Windows.Speech;
using UnityEngine.XR.WSA;
using Microsoft.Azure.SpatialAnchors.Unity;
using HoloToolkit.Unity.SpatialMapping;

public class TestScript : MonoBehaviour
{
    public GameObject CADPrefab = null;
    public bool AppSetsAnchor = false;

    string ACCOUNT_ID;
    string ACCOUNT_KEY;
    string SHARING_URL;

    string BaseSharingUrl;
    CloudSpatialAnchorSession cloudAnchorSession;
    CloudSpatialAnchorWatcher cloudAnchorWatcher;
    CloudSpatialAnchor cloudAnchor;
    AnchorExchanger anchorExchanger = new AnchorExchanger();

    GameObject Locator;


    /// <summary>
    /// Our queue of actions that will be executed on the main thread.
    /// </summary>
    private readonly Queue<Action> dispatchQueue = new Queue<Action>();

    bool _createAnchorFromCloud = false, _anchorCreatedFromCloud = false;

    void Start()
    {
        Locator = FindObjectOfType<MoveByGaze>().gameObject;
        AzureSpatialAnchorsConnectionConfig azureConfig = Resources.Load<AzureSpatialAnchorsConnectionConfig>("AzureSpatialAnchorsConnectionConfig");
        ACCOUNT_ID = azureConfig.SpatialAnchorsAccountId;
        ACCOUNT_KEY = azureConfig.SpatialAnchorsAccountKey;
        BaseSharingUrl = azureConfig.BaseSharingURL;

        Uri result;
        if (!Uri.TryCreate(BaseSharingUrl, UriKind.Absolute, out result))
        {
            Debug.Log("BaseSharingUrl, on AzureSpatialAnchorsDemoConfig in Examples/Resources, is not a valid url");
            return;
        }
        else
        {
            BaseSharingUrl = $"{result.Scheme}://{result.Host}/api/anchors";
        }

        CreateSession();
    }

    void Update()
    {
        lock (dispatchQueue)
        {
            if (dispatchQueue.Count > 0)
            {
                dispatchQueue.Dequeue()();
            }
        }
    }


    void CreateSession()
    {
        if (this.cloudAnchorSession == null)
        {
            this.cloudAnchorSession = new CloudSpatialAnchorSession();
            this.cloudAnchorSession.Configuration.AccountId = ACCOUNT_ID.Trim();
            this.cloudAnchorSession.Configuration.AccountKey = ACCOUNT_KEY.Trim();
            this.cloudAnchorSession.LogLevel = SessionLogLevel.All;
            this.cloudAnchorSession.Error += ((s, e) => Debug.LogError("ASA Error: " + e.ErrorMessage));

            this.cloudAnchorSession.AnchorLocated += ((object sender, AnchorLocatedEventArgs args) =>
            {
                switch (args.Status)
                {
                    case LocateAnchorStatus.Located:
                        CloudSpatialAnchor foundAnchor = args.Anchor;
                        // Go add your anchor to the scene...
                        //if (foundAnchor.Identifier == this.cloudAnchor.Identifier)
                        //{
                            //this._createAnchorFromCloud = true;
                            QueueOnUpdate(() =>
                            {
                                this.PlaceCADFromAnchor(foundAnchor);
                            });
                        //}
                        
                        
                        break;
                    case LocateAnchorStatus.AlreadyTracked:
                        // This anchor has already been reported and is being tracked
                        Debug.Log("ASA Info: Anchor already tracked. Identifier: " + args.Identifier);
                        break;
                    case LocateAnchorStatus.NotLocatedAnchorDoesNotExist:
                        // The anchor was deleted or never existed in the first place
                        // Drop it, or show UI to ask user to anchor the content anew
                        Debug.LogError("ASA Error: Anchor not located does not exist. Identifier: " + args.Identifier);
                        break;
                    case LocateAnchorStatus.NotLocated:
                        // The anchor hasn't been found given the location data
                        // The user might in the wrong location, or maybe more data will help
                        // Show UI to tell user to keep looking around
                        Debug.Log("ASA Info: Anchor not located. Identifier: " + args.Identifier);
                        break;
                }
            });
            


            this.cloudAnchorSession.LocateAnchorsCompleted += ((object sender, LocateAnchorsCompletedEventArgs args) =>
            {
                Debug.Log("Completed locating anchors");
            }) ;

            this.cloudAnchorSession.SessionUpdated += ((object sender, SessionUpdatedEventArgs args) =>
            {
                var status = args.Status;
                if (status.UserFeedback == SessionUserFeedback.None) return;
                Debug.Log($"Feedback: {Enum.GetName(typeof(SessionUserFeedback), status.UserFeedback)} -" +
                    $" Recommend Create={status.RecommendedForCreateProgress: 0.#%}");
            });

            this.cloudAnchorSession.Start();
            Debug.Log("Session started");

            this.anchorExchanger.WatchKeys(BaseSharingUrl);

            if (!this.AppSetsAnchor)
            {

                Task task = new Task( () =>
                {
                    while (this.anchorExchanger.AnchorKeys.Count < 1)
                    {

                    }
                    //cloudAnchor = await this.cloudAnchorSession.GetAnchorPropertiesAsync(this.anchorExchanger.AnchorKeys[0]);
                    //Debug.Log(cloudAnchor.Identifier + " found on cloud");
                    AnchorLocateCriteria criteria = new AnchorLocateCriteria();
                    criteria.Identifiers = this.anchorExchanger.AnchorKeys.ToArray();
                    this.cloudAnchorSession.CreateWatcher(criteria);
                    
                });
                task.Start();              

            }
        }
    }

    void PlaceCADFromAnchor(CloudSpatialAnchor anchor)
    {
        if (CADPrefab != null)
        {
            Locator.transform.position = Vector3.zero;

            if (float.TryParse(anchor.AppProperties[@"Size"], out float scaleVal))
            {
                Locator.transform.localScale = Vector3.one * scaleVal;
            }
            else scaleVal = 1.0f;
            Debug.Log(anchor.AppProperties[@"Rotation"]);
            string[] rotValuesString = anchor.AppProperties[@"Rotation"].Split(new char[] { '(', ',', ' ', ')' }, StringSplitOptions.RemoveEmptyEntries);
            //foreach (string s in rotValuesString)
            //{
            //    Debug.Log(s);
            //}
            float[] rotvalues = new float[rotValuesString.Length];
            for (int str = 0; str < rotValuesString.Length; str++)
            {
                if (float.TryParse(rotValuesString[str], out rotvalues[str]))
                {
                    //Debug.Log(str + " " + rotvalues[str]);
                }
                else
                {
                    Debug.Log("Failed to parse " + rotValuesString[str]);
                }
            }
            Locator.transform.localEulerAngles = new Vector3(rotvalues[0], rotvalues[1], rotvalues[2]);

            Locator.SetActive(true);
            //Debug.Log("set active");

            if (Locator.GetComponent<MoveByGaze>() != null)
            {
                Locator.GetComponent<MoveByGaze>().tester = this;
                //Debug.Log("test defined");
            }

            Locator.AddComponent<WorldAnchor>();
            //Debug.Log("anchor added");

            // Get the WorldAnchor from the CloudSpatialAnchor and use it to position the CAD.
            Locator.GetComponent<WorldAnchor>().SetNativeSpatialAnchorPtr(anchor.LocalAnchor);

            RaycastHit hitInfo;
            if ((Physics.Raycast(Locator.transform.position, Vector3.down, out hitInfo, 0.5f, SpatialMappingManager.Instance.LayerMask))
                || (Physics.Raycast(Locator.transform.position, Vector3.up, out hitInfo, 0.5f, SpatialMappingManager.Instance.LayerMask)))
            {
                Locator.transform.GetChild(0).GetChild(1).GetComponentInChildren<TMPro.TextMeshProUGUI>().text = (Locator.transform.position - hitInfo.point).ToString();
                Locator.RemoveARAnchor();
                Locator.transform.position = hitInfo.point;
                Locator.AddComponent<WorldAnchor>();
            }
            else Debug.Log("No offset");

            Locator.transform.GetChild(0).GetChild(1).GetComponentInChildren<TMPro.TextMeshProUGUI>().text =
                (Locator.transform.localScale.ToString() + "\n" + Locator.transform.localEulerAngles);
            this._anchorCreatedFromCloud = true;
        }
    }

    /// <summary>
    /// Queues the specified <see cref="Action"/> on update.
    /// </summary>
    /// <param name="updateAction">The update action.</param>
    protected void QueueOnUpdate(Action updateAction)
    {
        lock (dispatchQueue)
        {
            dispatchQueue.Enqueue(updateAction);
        }
    }


    public async Task OnCreateAnchorAsync(GameObject CAD)
    {
        WorldAnchor worldAnchor = CAD.GetComponent<WorldAnchor>();

        cloudAnchor = new CloudSpatialAnchor();
        cloudAnchor.LocalAnchor = CAD.GetNativeAnchorPointer();
        cloudAnchor.Expiration = DateTimeOffset.Now.AddDays(7);
        cloudAnchor.AppProperties.Add(@"Size", CAD.transform.localScale.x.ToString());
        cloudAnchor.AppProperties.Add(@"Rotation", CAD.transform.localEulerAngles.ToString());

        SessionStatus value = await this.cloudAnchorSession.GetSessionStatusAsync();
        Debug.Log(value.RecommendedForCreateProgress);
        while (value.RecommendedForCreateProgress < 1.0f)
        {
            value = await this.cloudAnchorSession.GetSessionStatusAsync();
            CAD.transform.GetChild(0).GetChild(1).GetComponentInChildren<TMPro.TextMeshProUGUI>().text = value.RecommendedForCreateProgress.ToString();
        }
        //if (value.RecommendedForCreateProgress < 1.0f) return;
        // Issue the creation request ...

        await this.cloudAnchorSession.CreateAnchorAsync(cloudAnchor);

        //CAD.name += cloudAnchor.Identifier;

        await anchorExchanger.StoreAnchorKey(cloudAnchor.Identifier);
        Debug.Log(CAD.name + " anchor created");
        Debug.Log($"Created a cloud anchor with ID={cloudAnchor.Identifier}");
        //this.SayAsync("cloud anchor created");

    }


    async Task WaitForSessionReadyToCreateAsync()
    {
        while (true)
        {
            var status = await this.cloudAnchorSession.GetSessionStatusAsync();

            if (status.ReadyForCreateProgress >= 1.0f)
            {
                break;
            }
            await Task.Delay(250);
        }
    }
}